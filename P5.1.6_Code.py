from flask import Flask
from flask_ask import Ask, statement, question
from random import randint
from dictionnary import *


app = Flask(__name__)
ask = Ask(app, '/')
test = False


@ask.launch
def start():
    return question("Nous allons jouer au QuotesGame. "
                    "Le but est simple, vous devez trouver de quels films ou de quelles séries proviennent "
                    "les répliques que je vais lire. "
                    "Dites 'suivant' pour passer à la réplique suivante et 'quit' pour quitter le jeu. "
                    "Joyeux QuotesGame et puisse le sort vous etre favorable.")


@ask.intent('GameIntent')
def game(word):

    quote = quotes()
    with open('answer.txt', 'a') as f:
        f.write(quote[0] + '\n')

    with open('answer.txt', 'r') as f:
        lines = f.readlines()
        save = lines[-2].strip()

    if word == 'suivant':
        global test
        test = True
        return question("D'où vient cette phrase: {}".format(quote[1]))

    if word == save:
        test = False
        correct = correct_words
        number = randint(0, len(correct) - 1)
        return question("{}".format(correct[number]))

    if word == 'quit':
        with open('answer.txt', 'w') as f:
            f.write('Usefull line'+"\n")
        return statement('Au revoir')

    if word is not None and test:
        test = False
        wrong = wrong_words
        number = randint(0, len(wrong) - 1)
        return question("{}, la bonne réponse était {}".format(wrong[number], save))
    else:
        return question("Mot invalide")



def quotes():

    text = quotes_dic
    movies = []
    for key in text:
        movies.append(key)

    number1 = randint(0, len(movies) - 1)
    movie_name = movies[number1]
    number2 = randint(0, len(text[movie_name]) - 1)

    return (movie_name, text[movie_name][number2])


if __name__ == '__main__':
    app.run()
